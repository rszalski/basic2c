%NAZWA
nazwa(X) --> slowo(X1), { string_to_list(X,X1) }.
slowo([L|Rest]) --> litera(L),slowo(Rest).
slowo([L]) --> litera(L).
litera(C) --> [C], {char_type(C,alnum)}.

%tekst
tekst(X) --> slowo_t(X1), { string_to_list(X,X1) }.
slowo_t([L|Rest]) --> litera_t(L),slowo_t(Rest).
slowo_t([L]) --> litera_t(L).
litera_t(C) --> [C], {char_type(C,csym)}.

%NAZWY
nazwy(X) --> nazwa(X1),",",nazwy(X2), { concat(X1,',',XW1), concat(XW1,X2,X) }.
nazwy(X) --> nazwa(X).

%WARTOSC,LICZBY,LICZBA	  LICZBY->LICZBA??
wartosc(X) --> liczby(X1), { string_to_list(X,X1) }.
liczby([L|Rest]) --> liczba(L),liczby(Rest).
liczby([L]) --> liczba(L).
liczba(C) --> [C], {char_type(C,digit)}.

%WYR_ARYTMET
wyr_arytmet(X) --> nazwa(X1), operator_arytm(X2), nazwa(X3), {concat(X1,X2,XW1), concat(XW1,X3,X)}.
wyr_arytmet(X) --> wywolanie(X).
wyr_arytmet(X) --> nazwa(X).

%OPERATOR_ARYTM
operator_arytm(X) --> "+", {X='+'}.
operator_arytm(X) --> "-", {X='-'}.
operator_arytm(X) --> "*", {X='*'}.
operator_arytm(X) --> "/", {X='/'}.
operator_arytm(X) --> "%", {X=' mod '}.

%TYP
typ(X) --> "int", {X='Integer'}.
typ(X) --> "double", {X='Double'}.
typ(X) --> "bool", {X='Boolean'}.
typ(X) --> "char", {X='char'}.
typ(X) --> "float", {X='Real'}.

%ZMIENNE
zmienne(X) --> zmienna(X).
zmienne(X) --> zmienna(X1), " ", zmienne(X2), {concat(X1,' ',XW1), concat(XW1,X2,X)}.

zmienne_arg(X) --> zmienna_arg(X1), ", ", zmienne_arg(X2), {concat(X1,'; ',XW1), concat(XW1,X2,X)}.
zmienne_arg(X) --> zmienna_arg(X).
zmienna_arg(X) --> typ(XT)," ",nazwa(XN), {concat(XN,':',P1),concat(P1,XT,X)}.

zmienne_str(X) --> zmienna_str(X1), "; ", zmienne_str(X2), {concat(X1,'; ',XW1), concat(XW1,X2,X)}.
zmienne_str(X) --> zmienna_str(X1),";" , {concat(X1,';',X)}.
zmienna_str(X) --> typ(XT)," ",nazwa(XN), {concat(XN,':',P1),concat(P1,XT,X)}.

%ZMIENNA
zmienna(X) --> "const ",typ(_)," ",nazwa(XN),"=",wartosc(XW),";", {concat('const ',XN,XN2), concat(XN2, '=', XN3), concat(XN3, XW,XN4), concat(XN4, ';',X)}. 
zmienna(X) --> typ(XT)," ",nazwy(XN),";", {concat('var ',XN,P),concat(P,':',P1),concat(P1,XT,P2),concat(P2,';',X)}.
zmienna(X) --> typ_wyliczeniowy(X).
zmienna(X) --> tablica(X).

%TYP_WYLICZENIOWY
typ_wyliczeniowy(XW) --> "enum ",nazwa(XN)," { ",nazwy(XNZ)," };",{concat('type ',XN,P),concat(P,' =(',P1),concat(P1,XNZ,P2),concat(P2,');',XW)}.

%PRZYPISANIE
przypisanie(X) --> nazwa(X1), "=", wyr_arytmet(X2),";", { concat(X1, ' := ',XW1), concat(XW1,X2,XW3), concat(XW3,';',X)}.
przypisanie_p(X) --> nazwa(X1), "=", wyr_arytmet(X2),";", { concat(X1, ' := ',XW1), concat(XW1,X2,X)}.

%WYWOLANIE
wywolanie(X) --> nazwa(X1), "();", { concat(X1,'();',X) }.
wywolanie(X) --> nazwa(X1), "(", nazwy(X2), ")", { concat(X1,'(',XW1), concat(XW1, X2,XW2), concat(XW2,')',X) }.
wywolanie(X) --> nazwa(X1), "(", nazwy(X2), ");", { concat(X1,'(',XW1), concat(XW1, X2,XW2), concat(XW2,');',X) }.


%INKREMENTACJA,DEKREMENTACJA
inkrementacja(X) --> nazwa(XN),"++;",{concat('inc(',XN,P),concat(P,');',X)}.
dekrementacja(X) --> nazwa(XN),"--;",{concat('dec(',XN,P),concat(P,');',X)}.

%TABLICA
tablica(X) --> typ(XT)," ",nazwa(XN),"[",wartosc(XW),"]",";", {concat('type ',XN,XY),concat(XY,' = array[1..',XY1),concat(XY1,XW,XY2),concat(XY2,'] of ',XY3),concat(XY3,XT,XY4), concat(XY4,';',X)}.

%STRUKTURA
struktura(X) -->"struct ",nazwa(XN)," { ",zmienne_str(XZ)," };",{concat('type ',XN,P),concat(P,' = record ',P1),concat(P1,XZ,P2),concat(P2,' end;',X)}.

%OPERATOR_REL
operator_rel(X) --> "==", {X='='}.
operator_rel(X) --> "!=", {X='<>'}.
operator_rel(X) --> "<", {X='<'}.
operator_rel(X) --> ">", {X='>'}.
operator_rel(X) --> "<=", {X='<='}.
operator_rel(X) --> ">=", {X='>='}.

%WARUNEK
warunek(X) --> nazwa(X1), operator_rel(X2), nazwa(X3), {concat(X1,X2,XW1), concat(XW1,X3,X)}.
warunek(X) --> nazwa(X1), op_logiczne(X2), nazwa(X3), {concat(X1,X2,XW1), concat(XW1,X3,X)}.
warunek(X) --> op_logiczne(X1), nazwa(X2), {concat(X1,X2,X)}.
warunek(X) --> nazwa(X).

%op_logiczne
op_logiczne(X) --> "&&", {X=' and '}.
op_logiczne(X) --> "||", {X=' or '}.
op_logiczne(X) --> "^", {X=' xor '}.
op_logiczne(X) --> "!", {X='not '}.

%INSTR_PROSTA
instr_prosta(X) --> printf(X).
instr_prosta(X) --> scanf(X).
instr_prosta(X) --> przypisanie(X).
instr_prosta(X) --> wywolanie(X).
instr_prosta(X) --> inkrementacja(X).
instr_prosta(X) --> dekrementacja(X).
instr_prosta(X) --> petla(X).
instr_prosta(X) --> instr_warunkowa(X).
instr_prosta(X) --> wyr_arytmet(X).

%INSTR_ZL
instr_zl(X) --> instr_prosta(X1), " ", instr_zl(X2), {concat(X1,' ',XW1), concat(XW1,X2,X)}.
instr_zl(X) --> instr_prosta(X).

%INSTR_ZLOZONE
instr_zlozone(X) --> "{ ",instr_zl(X1)," }",{concat('begin ',X1,P),concat(P,' end;',X)}.
instr_zlozoneif(X) --> "{ ",instr_zl(X1)," }",{concat('begin ',X1,P),concat(P,' end',X)}.

%INSTR_WARUNKOWA
instr_warunkowa(X) --> ifthen(X).
instr_warunkowa(X) --> ifthenelse(X).
ifthen(X) --> "if(", warunek(X1),")",instr_zlozone(X2), {concat('if(',X1,XW), concat(XW,')',XW1), concat(XW1,' then ',XW2), concat(XW2,X2,X)}.
ifthenelse(X) --> "if(", warunek(X1),")",instr_zlozoneif(X2)," else ",instr_zlozone(X3), {concat('if(',X1,XW), concat(XW,')',XW1), concat(XW1,' then ',XW2), concat(XW2,X2,XW3), concat(XW3,' else ',XW4), concat(XW4,X3,X)}.

%PETLA
petla(X) --> while(X).
petla(X) --> dowhile(X).
petla(X) --> for(X).
for(X) --> "for(i=",nazwa(X1),";","i<=",nazwa(X2),";i++)", instr_zlozone(X4), {concat('for i:=',X1,XW), concat(XW,' to ',XW1), concat(XW1,X2,XW2), concat(XW2,' do ',XW3), concat(XW3, X4, X)}.
dowhile(X) --> "do ",instr_zlozone(IZ)," while(",warunek(XW),");",{concat('repeat ',IZ,P),concat(P,' until (',P1),concat(P1,XW,P2),concat(P2,');',X)}.
while(X) --> "while(",warunek(W),")",instr_zlozone(IZ),{concat('while (',W,P),concat(P,') do ',P1),concat(P1,IZ,X)}.

%DEF_FUNKCJI,PROCEDURY
def_funkcji(X) --> typ(XT)," ",nazwa(XN),"(",argfunkcji(XZ),"){ ",instr_zl(IZ)," return ",nazwa(XR),"; }",{concat('function ',XN,P),concat(P,'(',P1),concat(P1,XZ,P2),concat(P2,')',P3),concat(P3,':',P4),concat(P4,XT,P5),concat(P5,'; ',P6), concat(P6,'begin ',P7), concat(P7,IZ,P8), concat(P8, ' ',P9), concat(P9,XN,P10), concat(P10, ':=',P11), concat(P11,XR,P12), concat(P12,'; end;',X)}.
def_funkcji(X) --> typ(XT)," ",nazwa(XN),"(",argfunkcji(XZ),"){ ",zmienne(XM)," ", instr_zl(IZ)," return ",nazwa(XR),"; }",{concat('function ',XN,P),concat(P,'(',P1),concat(P1,XZ,P2),concat(P2,')',P3),concat(P3,':',P4),concat(P4,XT,P5),concat(P5,'; ',P6), concat(P6,XM,P7), concat(P7,' begin ',P8), concat(P8, IZ,P9), concat(P9,' ',P10), concat(P10,XN,P11), concat(P11, ':=',P12), concat(P12,XR,P13), concat(P13,'; end;',X)}.

def_procedury(X) --> "void ", nazwa(X1),"(",argfunkcji(X2), ")",instr_zlozone(X3),{concat('procedure ', X1, XW), concat(XW,'(',XW1), concat(XW1, X2, XW2), concat(XW2,'); ',XW3),concat(XW3, X3, X)}.

%ARGFUN
argfunkcji(X) --> zmienne_arg(X).
argfunkcji(X) --> "", { X = "" }.

%ZLOZENIA FUNKCJI i PROCEDUR
fun(X) --> def_funkcji(X1), " ", fun(X2), {concat(X1,' ',XW1), concat(XW1,X2,X)}.
fun(X) --> def_procedury(X1), " ", fun(X2), {concat(X1,' ',XW1), concat(XW1,X2,X)}.
fun(X) --> def_funkcji(X).
fun(X) --> def_procedury(X).
fun(X) --> struktura(X1), " ", fun(X2), {concat(X1,' ',XW1), concat(XW1,X2,X)}.
fun(X) --> struktura(X).

%PROGRAM
program(X) --> main(X).
program(X) --> fun(X1), " ", main(X2), {concat(X1,' ',XW1), concat(XW1,X2,X)}.
program(X) --> zmienne(X1), " ", fun(X2), " ", main(X3), {concat(X1,X2,XW1),concat(XW1,X3,X)}.
program(X) --> zmienne(X1), " ", main(X2), {concat(X1,X2,X)}.

%MAIN
main(X) --> "void main(){ ",zmienne(X1), " ", instr_zl(X2), " }", { concat(X1,' begin ',XW1), concat(XW1,X2,XW2), concat(XW2,' end.',X)}.
main(X) --> "void main(){ ",zmienne(X1), " }", { concat(X1,' begin ',XW1), concat(XW1,' end.',X)}.
main(X) --> "void main(){ ",instr_zl(X2), " }", { concat('begin ',X2,XW1), concat(XW1,' end.',X)}.

%PRINTF
printf(X) --> "printf(", nazwa(XN), ");", {concat('writeln(', XN, XW), concat(XW, ');', X)}.
printf(X) --> "printf(", "\"", tekst(XN), "\"", ");", {concat('writeln(\'', XN, XW), concat(XW, '\');', X)}.

%SCANF
scanf(X) --> "scanf(&", nazwa(X1), ");", {concat('readln(', X1, XW), concat(XW, ');', X)}. 


%TESTY I FORMAT ZAPISU

% warunek(X,"a==b",[]).
% warunek(X,"a||b",[]).
% warunek(X,"a",[]).

% def_funkcji(X,"double fun(int a, char b){ a++; }",[]).

% przypisanie(X,"a=5;",[]),write(X),nl.
% wyr_arytmet(X,"a+7;",[]),write(X),nl.
% tablica(X,"char tab[5];",[]),write(X),nl.

% wyr_arytmet(X,"a+6",[]).
% wyr_arytmet(X,"a%6",[]).

% zmienna(X,"double f;",[]).
% zmienna(X,"const char f=7;",[]).

% typ_wyliczeniowy(X,"enum wyl {f,b,z}",[]).

% zmienne(X,"double f,b;",[]).
% zmienne(X,"double f; int a;",[]).
% zmienne_arg(X,"double f, int a",[]).

% przypisanie(X,"x=y;",[]).
% przypisanie(X,"x=7*8;",[]).

% wywolanie(X,"funkcja();",[]).
% wywolanie(X,"funkcja(par1,par2);",[]).

% inkrementacja(X,"x++;",[]).
% dekrementacja(X,"x--;",[]).

% tablica(X,"char tab[7];",[]).

% struktura(X,"struct konto { int nrKonta; char imie; char nazwisko; float kwota; };",[]).

% warunek(X,"a>5",[]).
% warunek(X,"a||b",[]).
% warunek(X,"!a",[]).

% instr_warunkowa(X,"if(a==b){ a++; }",[]).
% instr_warunkowa(X,"if(a==b){ a++; } else{ b++; }",[]).

% petla(X,"for(i=1;i<=5;i++){ y=5; }",[]).
% petla(X,"do { y++; } while(i!=5);",[]).
% petla(X,"while (x!=7){ funkcja(); }",[]).

% def_procedury(X,"void funkcja(int a, char c){ a++; }",[]).
% def_funkcji(X,"int plus(int a){ a++; return a; }",[]),write(X),nl.
% def_funkcji(X,"int plus(int a){ int c; c=a; c++; return a; }",[]),write(X),nl.

% program(X,"void main(){ int a; a=5; }",[]),write(X),nl.
% program(X,"void main(){ int a; a=a+7; }",[]),write(X),nl.

test1 :- program(X,"void main(){ printf(\"projekt\"); }",[]),write(X),!.
test2 :- program(X,"int plus(int a){ int c; c=a; c++; return a; } void main(){ int x; x=plus(5); }",[]),write(X),!.


dcg :-
    open('out.c',read,R),
    read_line_to_codes(R,X),
    close(R),
	program(P,X,[]),
	open('out.pas', write, W),
	string_to_list(F,P),
	write(W, F),
	close(W),
	write('Ukonczono').

:- dcg.