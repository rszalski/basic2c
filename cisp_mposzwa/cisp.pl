type --> typemod, space, typesign, space, typename.
type --> typesign, space, typemod, space, typename.
type --> typesign, space, typename.
type --> typemod, space, typename.
type --> typename.
type --> typesign.
type --> typemod.
type --> "void".
type --> "float".
type --> "double".
typename --> "int".
typename --> "char".
typemod --> "short".
typemod --> "long".
typemod --> "long", space, "long".
typesign --> "signed".
typesign --> "unsigned".

application(Z) --> function(X), whitespace, application(Y), {swritef(Z, "%t %t",[X,Y])}.
application(Z) --> function(Z), whitespace.

block(Z) --> "{", whitespace, instructionlist(X), whitespace, "}", {swritef(Z, "(begin %t)", [X])}.
function(Z) --> type, space, string(Name),
	"(", whitespace, functionargs(X), whitespace, ")", whitespace, functionblock(Y),
	{swritef(Z, "(define (%t %t) %t)", [Name, X, Y])}.

functionblock(Z) --> "{", whitespace, "return", space, expression(Z), ";", whitespace, "}",!.
functionblock(Z) --> "{", whitespace, instructionlist(X), "return ", expression(E), ";", whitespace, "}",
	{swritef(Z, "(begin %t %t)", [X,E])},!.
functionblock(Z) --> "{", whitespace, declarations(Vars), instructionlist(X), "return ", expression(E), ";", whitespace, "}",
	{swritef(Z, "(let (%t) %t %t)", [Vars,X,E])}, !.
functionargs(Z) --> functionarg(X), whitespace, ",", whitespace, functionargs(Y), {swritef(Z, "%t %t", [X,Y])}.
functionargs(Z) --> functionarg(Z).
functionargs(Z) --> {swritef(Z, "%s", [""])}.

functionarg(Z) --> type, space, string(M), {swritef(Z, "%t", [M])}, !.

functioncall(Z) --> string(FuncName), whitespace, "(", whitespace, functioncallargs(E), whitespace, ")", {swritef(Z,"(%t %t)",[FuncName,E])}.

functioncallargs(Z) --> expression(E), whitespace, ",", whitespace, functioncallargs(Y), {swritef(Z, "%t %t", [E,Y])}.
functioncallargs(Z) --> expression(Z).
functioncallargs(Z) --> {swritef(Z,"%s", [""])}.

instructionlist(Z) --> instruction(T), whitespace, instructionlist(X), {swritef(Z, "%t %t", [T,X])},!.
instructionlist(Z) --> instruction(Z),!.
instructionlist(Z) --> {swritef(Z,"%s",[""])}.

instruction(Z) --> assignment(Z).
instruction(Z) --> whileloop(Z).
instruction(Z) --> ifelse(Z).
instruction(Z) --> if(Z).
instruction(Z) --> expression(Z),";".

whileloop(Z) --> "while(", conditional(X), ")",whitespace, block_or_instr(Y), {swritef(Z, "(while %t %t)", [X,Y])}, !.

declarations(Z) --> declaration(X), whitespace, declarations(Y), {swritef(Z, "%t%t", [X, Y])},!.
declarations(Z) --> declaration(Z), !.
declarations(Z) --> {swritef(Z, "%s", [""])}.

declaration(Z) --> type, space, string(M), ";", {swritef(Z, "(%t %s)", [M, "0"])}, !.

assignment(Z) --> string(Var), whitespace, "=", whitespace, expression(E),";", {swritef(Z, "(set! %t %t)", [Var, E])}.

expr(Z) --> string(Z).
expr(Z) --> functioncall(Z).

cond(Z) --> conditional(Condition), whitespace,"?", whitespace, expression(Iftrue), whitespace,":", whitespace, expression(Iffalse),
	{swritef(Z,"(if %t %t %t)", [Condition, Iftrue, Iffalse])}.

block_or_instr(Z) --> instruction(Z).
block_or_instr(Z) --> block(Z).

ifelse(Z) -->
	"if", whitespace, "(", whitespace,	conditional(C), whitespace, ")",
	whitespace,	block_or_instr(I),
	whitespace, "else", whitespace, block(B),
	{swritef(Z,"(if %t %t %t)", [C, I, B])}.
ifelse(Z) -->
	"if", whitespace, "(", whitespace,	conditional(C), whitespace, ")",
	whitespace,	block_or_instr(I),
	whitespace, "else", space, instruction(E),
	{swritef(Z,"(if %t %t %t)", [C, I, E])}.
if(Z) -->
	"if", whitespace, "(", whitespace,	conditional(C), whitespace, ")",
	whitespace,	block_or_instr(B),
	{swritef(Z,"(if %t %t)", [C, B])}.

expression(Z) --> expr(Z).
expression(Z) --> cond(Z).
expression(Z) --> expr(A), whitespace, "+", whitespace, expression(B), {swritef(Z, "(+ %t %t)", [A,B])}.
expression(Z) --> expr(A), whitespace, "-", whitespace, expression(B), {swritef(Z, "(- %t %t)", [A,B])}.
expression(Z) --> "-", whitespace, expression(B), {swritef(Z, "(- %t)", [B])}.
expression(Z) --> expr(A), whitespace, "*", whitespace, expression(B), {swritef(Z, "(* %t %t)", [A,B])}.
expression(Z) --> expr(A), whitespace, "/", whitespace, expression(B), {swritef(Z, "(/ %t %t)", [A,B])}.
expression(Z) --> "(", whitespace, expression(A), whitespace, ")", {swritef(Z, "%t", [A])}.


conditional(Z) --> expr(A), whitespace, ">", whitespace, expression(B), {swritef(Z, "(> %t %t)", [A,B])}.
conditional(Z) --> expr(A), whitespace, "<", whitespace, expression(B), {swritef(Z, "(< %t %t)", [A,B])}.
conditional(Z) --> expr(A), whitespace, ">=", whitespace, expression(B), {swritef(Z, "(>= %t %t)", [A,B])}.
conditional(Z) --> expr(A), whitespace, "<=", whitespace, expression(B), {swritef(Z, "(<= %t %t)", [A,B])}.
conditional(Z) --> expr(A), whitespace, "==", whitespace, expression(B), {swritef(Z, "(eq? %t %t)", [A,B])}.
conditional(Z) --> expr(A), {swritef(Z, "(not (eq? 0 %t))", [A])}.

space --> " ", whitespace.
space --> "\n", whitespace.
space --> "\t", whitespace.

whitespace --> "\t", whitespace.
whitespace --> "\n", whitespace.
whitespace --> " ", whitespace.
whitespace --> "".

string(C) --> characters(A), {swritef(C,"%s",[A])}.
characters([D|[]]) --> character(D).
characters([D|Ds]) --> character(D), characters(Ds).
character(D) --> [D], {code_type(D, alnum)}.

c2scheme:-
	write('Witaj w translatorze języka C do Scheme!\n'),
	write('Podaj ścieżkę do pliku wejściowego ze źródłem programu w C:\n'),
	read(Input),
	read_from_file(Input,S),
	write('Program przetłumaczony pomyślnie!\n'),
	write('Podaj ścieżkę do pliku w którym ma zostać zapisany program w Scheme:\n'),
	read(Output),
	save_to_file(Output, S),
	write('Operacje zakończone.').

read_from_file(P,S):-
	read_file_to_codes(P,C,""),
	application(S,C,[]).

save_to_file(P,S):-
	open(P,write,Stream),
	write(Stream, S),
	close(Stream).